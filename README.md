# README #

Custom Open Graph Image in Opencart.

### What is this repository for? ###

This extension adds functionality to have a custom OpenGraph Image for products. If not present, it will use the main image.

### How do I get set up? ###

OpenCart 2.2.0.0.

Just extract archive and upload to you server. No files are replaced.
You should ALWAYS back up your data before modifying it.

You must add a column to the product table in the database:

```
ALTER TABLE `oc_product` ADD `ogimage` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL AFTER `image`;	
```

To use this mod you also need VQMod installed and this extension:

```
https://www.opencart.com/index.php?route=marketplace/extension/info&extension_id=22963
```
In the OpenGraph-extension modify the following:

```
if ($product_info['image']) {
   $this->document->addOGMeta('property="og:image"', str_replace(' ', '%20', $this->config->get('config_url') . $this->model_tool_image->resize($product_info['image'], 1200, 630)) );
   $this->document->addOGMeta('property="og:image:width"', '1200');
   $this->document->addOGMeta('property="og:image:height"', '630');
} else {
   $this->document->addOGMeta( 'property="og:image"', str_replace(' ', '%20', $this->config->get('config_url') . $this->model_tool_image->resize($this->config->get('config_logo'), 1200, 630)) );
   $this->document->addOGMeta('property="og:image:width"', '1200');
   $this->document->addOGMeta('property="og:image:height"', '630');
}
foreach ($results as $result) {
   $this->document->addOGMeta( 'property="og:image"', str_replace(' ', '%20', $this->config->get('config_url') . $this->model_tool_image->resize($result['image'], 1200, 630)) );
   $this->document->addOGMeta('property="og:image:width"', '1200');
   $this->document->addOGMeta('property="og:image:height"', '630');
}
```

To this:

```
if ($product_info['ogimage']) { 
   $this->document->addOGMeta('property="og:image"', str_replace(' ', '%20', $this->config->get('config_url') . $this->model_tool_image->resize($product_info['ogimage'], 1200, 630)) );
   $this->document->addOGMeta('property="og:image:width"', '1200');
   $this->document->addOGMeta('property="og:image:height"', '630');
} else if ($product_info['image']) {
   $this->document->addOGMeta('property="og:image"', str_replace(' ', '%20', $this->config->get('config_url') . $this->model_tool_image->resize($product_info['image'], 1200, 630)) );
   $this->document->addOGMeta('property="og:image:width"', '1200');
   $this->document->addOGMeta('property="og:image:height"', '630');
} else {
   $this->document->addOGMeta( 'property="og:image"', str_replace(' ', '%20', $this->config->get('config_url') . $this->model_tool_image->resize($this->config->get('config_logo'), 1200, 630)) );
   $this->document->addOGMeta('property="og:image:width"', '1200');
   $this->document->addOGMeta('property="og:image:height"', '630');
}
foreach ($results as $result) {
   $this->document->addOGMeta( 'property="og:image"', str_replace(' ', '%20', $this->config->get('config_url') . $this->model_tool_image->resize($result['image'], 1200, 630)) );
   $this->document->addOGMeta('property="og:image:width"', '1200');
   $this->document->addOGMeta('property="og:image:height"', '630');
}
```


### Contribution guidelines ###

Please feel free to comment with suggestions.

### Who do I talk to? ###

Please test and give me some feedback if something does not work. Since we give this away for free we do not give you any guaranties for functionality. We also do NOT make customized versions of this mod. Hire a developer... 